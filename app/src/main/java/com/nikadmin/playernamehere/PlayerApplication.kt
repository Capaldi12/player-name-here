package com.nikadmin.playernamehere

import android.app.Application
import io.paperdb.Paper

class PlayerApplication : Application() {
    override fun onCreate() {
        Paper.init(applicationContext)

        super.onCreate()
    }
}