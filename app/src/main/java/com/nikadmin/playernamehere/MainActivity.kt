package com.nikadmin.playernamehere

import android.os.Bundle
import android.Manifest
import android.util.Log

import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast

import androidx.activity.result.contract.ActivityResultContracts
import android.content.pm.PackageManager.PERMISSION_GRANTED


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val havePermission = checkStoragePermission()

        Toast.makeText(
            applicationContext,
            "Have permission: $havePermission",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun checkStoragePermission() : Boolean {
        if (applicationContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            == PERMISSION_GRANTED)

            return true

        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE))

            Toast.makeText(
                applicationContext,
                "We need access to storage to play your music",
                Toast.LENGTH_LONG
            ).show()

        var res = false

        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                    isGranted: Boolean ->

                if (isGranted) {
                    res = true
                    Log.i("Permission: ", "Granted")
                } else {
                    Log.i("Permission: ", "Denied")
                }
            }

        requestPermissionLauncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)

        return res
    }
}
